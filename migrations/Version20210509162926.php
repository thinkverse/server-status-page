<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210509162926 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE status_service (status_id INT NOT NULL, service_id INT NOT NULL, INDEX IDX_E94DED576BF700BD (status_id), INDEX IDX_E94DED57ED5CA9E6 (service_id), PRIMARY KEY(status_id, service_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE status_service ADD CONSTRAINT FK_E94DED576BF700BD FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE status_service ADD CONSTRAINT FK_E94DED57ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX IDX_E19D9AD26BF700BD ON service');
        $this->addSql('ALTER TABLE service DROP status_id');
        $this->addSql('ALTER TABLE status DROP FOREIGN KEY FK_7B00651CAEF5A6C1');
        $this->addSql('DROP INDEX IDX_7B00651CAEF5A6C1 ON status');
        $this->addSql('ALTER TABLE status DROP services_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE status_service');
        $this->addSql('ALTER TABLE service ADD status_id INT DEFAULT NULL');
        $this->addSql('CREATE INDEX IDX_E19D9AD26BF700BD ON service (status_id)');
        $this->addSql('ALTER TABLE status ADD services_id INT NOT NULL');
        $this->addSql('ALTER TABLE status ADD CONSTRAINT FK_7B00651CAEF5A6C1 FOREIGN KEY (services_id) REFERENCES service (id)');
        $this->addSql('CREATE INDEX IDX_7B00651CAEF5A6C1 ON status (services_id)');
    }
}
