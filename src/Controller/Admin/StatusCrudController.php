<?php

namespace App\Controller\Admin;

use App\Entity\Status;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class StatusCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return Status::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Status')
            ->setEntityLabelInPlural('Statuses')
            ->setSearchFields(['type_id', 'title', 'text'])
            ->setDefaultSort(['end_time' => 'ASC'])
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
        ;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('services'))
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new('services')
            ->hideOnDetail()
        ;
        yield ArrayField::new('services')
            ->onlyOnDetail()
        ;
        yield ChoiceField::new('type_id')
            ->setChoices([
                'Operational' => 0,
                'Planned maintenance' => 1,
                'Minor outage' => 2,
                'Major outage' => 3,
            ]);
        yield TextField::new('title');
        yield TextEditorField::new('text');
        yield DateTimeField::new('start_time')
            ->hideOnIndex()
            ->renderAsChoice()
            ->setFormTypeOption('view_timezone', 'Europe/Berlin')
        ;
        yield DateTimeField::new('end_time')
            ->renderAsChoice()
            ->setFormTypeOption('view_timezone', 'Europe/Berlin')
        ;
        yield TextField::new('user')
            ->setFormTypeOptions(['data' => $this->getUser()->getUsername()])
        ;
    }
}
