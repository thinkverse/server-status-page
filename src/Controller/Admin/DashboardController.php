<?php

namespace App\Controller\Admin;

use App\Entity\Admin;
use App\Entity\Service;
use App\Entity\ServiceGroup;
use App\Entity\Status;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();
        $url = $routeBuilder->setController(StatusCrudController::class)->generateUrl();

        return $this->redirect($url);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Status Page');
    }

    public function configureMenuItems(): iterable
    {
#        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linktoRoute('Back to the website', 'fas fa-home', 'home');
        yield MenuItem::linkToCrud('ServiceGroup', 'fas fa-layer-group', ServiceGroup::class);
        yield MenuItem::linkToCrud('Service', 'fas fa-server', Service::class);
        yield MenuItem::linkToCrud('Status', 'fas fa-bell', Status::class);
        if ($this->isGranted('ROLE_ADMIN')) {
            yield MenuItem::linkToCrud('User', 'fas fa-user', Admin::class);
        }
    }
}
