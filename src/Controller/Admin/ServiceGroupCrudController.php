<?php

namespace App\Controller\Admin;

use App\Entity\ServiceGroup;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class ServiceGroupCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ServiceGroup::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Group')
            ->setEntityLabelInPlural('Groups')
            ->setSearchFields(['name', 'description'])
            ->setDefaultSort(['order_id' => 'ASC']);
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield NumberField::new('order_id');
        yield TextField::new('name');
        yield TextField::new('description')
            ->hideOnIndex()
        ;
    }
}
