<?php

namespace App\Controller\Admin;

use App\Entity\Service;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use Doctrine\Common\Collections\ArrayCollection;

class ServiceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Service::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Service')
            ->setEntityLabelInPlural('Services')
            ->setSearchFields(['name', 'description', 'url'])
            ->setDefaultSort(['serviceGroup' => 'ASC', 'order_id' => 'ASC'])
        ;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('serviceGroup'))
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new('serviceGroup');
        yield NumberField::new('order_id');
        yield TextField::new('lastStatusText', 'Last Status')
            ->hideOnForm()
        ;
        yield TextField::new('name');
        yield TextField::new('description')
            ->hideOnIndex()
        ;
        yield UrlField::new('url');
    }
}
