<?php

namespace App\Controller;

use App\Entity\Status;
use App\Repository\ServiceGroupRepository;
use App\Repository\ServiceRepository;
use App\Repository\StatusRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(ServiceGroupRepository $serviceGroupRepository, ServiceRepository $serviceRepository, StatusRepository $statusRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'servicegroups' => $serviceGroupRepository->findAllOrderedByOrderID(),
            'services' => $serviceRepository->findAllOrderedByOrderID(),
            'statuses' => $statusRepository->findAllOrderedByEnd_time(),
        ]);
    }

    #[Route('/status/{id}', name: 'status_delete', methods: ['POST'])]
    public function deleteStatus(Request $request, Status $status): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        if ($this->isCsrfTokenValid('delete'.$status->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($status);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home');
    }
}
