<?php
// src/Controller/PrivacyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrivacyController extends AbstractController
{
    /**
    * @Route("/privacy")
    */
    public function index(): Response
    {
        $privacyurl = "";
        $privacytext = "test";
        if ($privacyurl) {
            return $this->redirect($privacyurl);
        } else {
            return $this->render('/privacy/index.html.twig', [
                'privacytext' => $privacytext,
        ]);
        }
    }
}
