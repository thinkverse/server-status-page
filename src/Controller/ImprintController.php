<?php
// src/Controller/ImprintController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ImprintController extends AbstractController
{
    /**
    * @Route("/imprint")
    */
    public function index(): Response
    {
        $imprinturl = "https://rollenspiel.wiki";
        $imprinttext = "test";
        if ($imprinturl) {
            return $this->redirect($imprinturl);
        } else {
            return $this->render('/imprint/index.html.twig', [
                'imprinttext' => $imprinttext,
        ]);
        }
    }
}
