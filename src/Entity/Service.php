<?php

namespace App\Entity;

use App\Repository\ServiceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ServiceRepository::class)
 */
class Service
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity=ServiceGroup::class, inversedBy="services")
     */
    private $serviceGroup;

    /**
     * @ORM\Column(type="integer")
     */
    private $order_id;

    /**
     * @ORM\ManyToMany(targetEntity=Status::class, mappedBy="services", fetch="EAGER")
     */
    private $statuses;

    public function __construct()
    {
        $this->statuses = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getServiceGroup(): ?ServiceGroup
    {
        return $this->serviceGroup;
    }

    public function setServiceGroup(?ServiceGroup $serviceGroup): self
    {
        $this->serviceGroup = $serviceGroup;

        return $this;
    }

    public function getOrderId(): ?int
    {
        return $this->order_id;
    }

    public function setOrderId(int $order_id): self
    {
        $this->order_id = $order_id;

        return $this;
    }

    /**
     * @return Collection|Status[]
     */
    public function getStatuses(): Collection
    {
        return $this->statuses;
    }

    public function addStatus(Status $status): self
    {
        if (!$this->statuses->contains($status)) {
            $this->statuses[] = $status;
            $status->addService($this);
        }

        return $this;
    }

    public function removeStatus(Status $status): self
    {
        if ($this->statuses->removeElement($status)) {
            $status->removeService($this);
        }

        return $this;
    }

    public function getLastStatusId()
    {
        return $this->getStatuses()->last()->getTypeId();
    }

    public function getLastStatusClass()
    {
        $type_id = $this->getStatuses()->last()->getTypeId();

        if ($type_id == '0') {
            $statuses_class = 'success';
        }
        if ($type_id == '1') {
            $statuses_class = 'primary';
        }
        if ($type_id == '2') {
            $statuses_class = 'warning';
        }
        if ($type_id == '3') {
            $statuses_class = 'danger';
        }
        return $statuses_class;
    }

    public function getLastStatusText()
    {
        $type_id = $this->getStatuses()->last()->getTypeId();

        if ($type_id == '0') {
            $statuses_text = 'Operational';
        }
        if ($type_id == '1') {
            $statuses_text = 'Planned maintenance';
        }
        if ($type_id == '2') {
            $statuses_text = 'Minor outage';
        }
        if ($type_id == '3') {
            $statuses_text = 'Major outage';
        }
        return $statuses_text;
    }
}
